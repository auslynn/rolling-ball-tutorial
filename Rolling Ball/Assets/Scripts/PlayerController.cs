﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;
    private float movementZ;
    public float speed = 0;
    public float jumpHeight;
    public int numCubes;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    InputActions inputAction;
   // private bool onGround;
   // [SerializeField] private InputActionAsset playerControls;
    
    // Start is called before the first frame update
    
    void Awake()
    {
        inputAction = new InputActions();
       // jumpAction.performed += ctx => Jump(); //idk what this is
    }

    void OnEnable()
    {
        inputAction.Enable();
    }
    
    void OnDisable()
    {
        inputAction.Disable();
    }
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winTextObject.SetActive(false);
        Jump();
    }

     void Jump()
    {
        //Debug.Log("jump called");
        rb.AddForce(new Vector3(0, jumpHeight, 0));
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= numCubes)
        {
            winTextObject.SetActive(true);
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);
    }

    void Update()
    {
        if(transform.position.y == .5) // if player is on the ground then jump
        {
            if (inputAction.Player.Jump.triggered)
            {
                Debug.Log("jumped");
                Jump();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
            if(other.gameObject.tag == "PickUp")
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
            BoostSpeed(); //when the player picks up an object, they gain a boost in move speed to help them get to the next part of the level.
           // Debug.Log(speed);
        }

        if(other.gameObject.name == "BeamZone") // slows the player on the beam
        {
            speed = speed / 3;
        }

        if(other.gameObject.tag == "DeadZone") //resets players to the center if they fall off the beam
        {
            transform.position = (new Vector3 (0, .5f, 0));

        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name == "BeamZone") // returns the player to normal speed after leaving the beam
        {
            speed = speed * 3;
        }
    }

    void BoostSpeed() 
    {
        speed += .5f;
    }

    

    
}
